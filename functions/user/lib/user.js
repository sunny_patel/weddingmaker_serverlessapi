var user = {
  /**
   * Takes a user object, hashes the password and stores it in dynamo
   */
  new: function(user) {
    if (
      user.phone == null ||
      user.password == null ||
      user.firstName == null ||
      user.lastName == null ||
      user.type == null
    ) {
      return {
        "msg": "Missing property from user object.",
        "success": false
      };
    } else {
      // Valid user, attempt to save to dynamo
      
    }
  },
  test: function() {
    return true;
  }
};
module.exports = user;
